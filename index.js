module.exports = {
  extends: [
    // add more generic rulesets here, such as:
    // 'eslint:recommended',
    //"airbnb-vue",
    //"plugin:vue/recommended",
    "prettier",
    "plugin:vue/essential", // this is a default sub-set of rules for your .vue files
    "@vue/airbnb", // plug airbnb rules but made for .vue files
    "@vue/typescript" // default typescript related rules
  ],
  parserOptions: {
    ecmaVersion: 2018,
    parser: "@typescript-eslint/parser"
  },
  rules: {
    "no-empty-function": "off",
    "no-useless-constructor": "off",
    "no-use-before-define": ["error", { functions: false, classes: false }],
    "func-names": ["warn", "never"],
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": "error",
    "no-alert": "error",
    "linebreak-style": 0,
    "arrow-parens": ["error", "always"],
    "comma-dangle": [
      "error",
      {
        arrays: "always-multiline",
        objects: "always-multiline",
        imports: "always-multiline",
        exports: "always-multiline",
        functions: "always-multiline"
      }
    ],
    quotes: ["error", "single"],
    "no-unexpected-multiline": "off",
    semi: "off",
    "no-underscore-dangle": ["error", { allowAfterThis: true }],
    indent: ["error", 4, { SwitchCase: 1 }],
    "lines-between-class-members": [
      "error",
      "always",
      { exceptAfterSingleLine: true }
    ],
    "no-param-reassign": ["error", { props: false }],
    "no-undef": ["error", { typeof: true }],
    "no-unused-expressions": ["error", { allowShortCircuit: true }],
    "no-tabs": 0,
    "no-plusplus": ["error", { allowForLoopAfterthoughts: true }],
    "max-len":  ["error", { "code": 120 }],
    "object-curly-newline": [
      "error",
      {
        ObjectExpression: { multiline: true, consistent: true },
        ObjectPattern: { multiline: true, consistent: true },
        ImportDeclaration: { multiline: true, consistent: true },
        ExportDeclaration: { multiline: true, consistent: true }
      }
    ],
    // IMPORTS
    "import/prefer-default-export": "off",
    "import/first": ["error", "DISABLE-absolute-first"],
    "import/order": [
      "error",
      {
        groups: [
          ["builtin", "external", "internal"],
          "unknown",
          ["sibling", "parent"]
        ],
        "newlines-between": "always-and-inside-groups"
      }
    ],

    // TYPESCRIPT SPECIFIC

    "@typescript-eslint/no-useless-constructor": "error",
    "@typescript-eslint/array-type": "error",
    "@typescript-eslint/semi": ["error"],
    "@typescript-eslint/member-delimiter-style": [
      "error",
      {
        multiline: {
          delimiter: "semi",
          requireLast: true
        },
        singleline: {
          delimiter: "semi",
          requireLast: false
        }
      }
    ],

    // VUE SPECIFIC
    "vue/html-indent": ["error", 4],
    "vue/html-closing-bracket-spacing": ["error", { selfClosingTag: "always" }],
    "vue/html-quotes": ["error", "double"],
    "vue/html-closing-bracket-newline": [
      "error",
      {
        singleline: "never",
        multiline: "never"
      }
    ],
    "vue/max-attributes-per-line": "off",
    "vue/prop-name-casing": ["error", "camelCase"],
    "vue/no-v-html": "error",
    // TODO: Po vyreseni vsech warningu vue/require-default-prop a vue/require-prop-types, prepnout pravidlo na error
    "vue/require-default-prop": "error",
    "vue/require-prop-types": "error",
    "vue/valid-v-model": "error",
    "vue/no-multi-spaces": "error",
    "vue/require-v-for-key": "error",
    "vue/valid-v-html": "error",
    "vue/valid-v-bind": "error",
    "vue/valid-v-on": "error",
    "vue/component-name-in-template-casing": [
      "error",
      "kebab-case",
      {
        registeredComponentsOnly: true,
        ignores: []
      }
    ],
    // override/add rules settings here, such as:
    // 'vue/no-unused-vars': 'error'
    "@typescript-eslint/no-var-requires": "off",
    "class-methods-use-this": "off"
  },
  overrides: [
    {
      files: ["*.vue"],
      rules: {
        'max-len': 'off' // disables line length check
      }
    }
  ]
};

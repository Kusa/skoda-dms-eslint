# `@skoda-dms/eslint-config`

> Developement [Eslint](https://eslint.org/) config.

## Usage

**Install**:

```bash
$  npm install --save-dev @skoda-dms/eslint-config
```

**Edit `eslintrc`**:

```jsonc
{
  // ...
  "extends": "@skoda-dms/eslint-config"
}
```
